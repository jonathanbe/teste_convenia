<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Company;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'cnpj' => $faker->unique()->numberBetween($min = 11111111111111, $max = 99999999999999),
        'phone' => $faker->unique()->numberBetween($min = 111111111, $max = 999999999),
        'address' => $faker->streetAddress,
        'zipcode' => $faker->postcode,
    ];
});
