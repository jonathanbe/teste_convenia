<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Supplier;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->unique()->email,
        'monthly' => '50,00',
        'company_id' => 1
    ];
});
