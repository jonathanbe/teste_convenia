<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Services\UserService;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testCreate()
    {
        $userService = new UserService();

        $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => '123456',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'cnpj' => '34384271093',
            'phone' => '83998557854',
            'zipcode' => '58010-270'
        ]);

        $this->assertDatabaseHas('users', [
            'email' => 'jonathanbeltrao@gmail.com',
        ]);

        $this->assertDatabaseHas('companies', [
            'name' => 'Joninha Company',
        ]);
    }
}
