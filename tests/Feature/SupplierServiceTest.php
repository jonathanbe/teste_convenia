<?php

namespace Tests\Feature;

use App\Services\UserService;
use Tests\TestCase;
use App\Services\Api\SupplierService;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SupplierServiceTest extends TestCase
{

    use RefreshDatabase, WithFaker;

    public function testCreate()
    {
        $supplierService = new SupplierService();
        $userService = new UserService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => '123456',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'cnpj' => '34384271093',
            'phone' => '83998557854',
            'zipcode' => '58010-270'
        ]);

        $supplier = $supplierService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'supplier@gmail.com',
            'monthly' => '15,00',
        ], $user->company_id);

        $this->assertDatabaseHas('suppliers', [
            'name' => 'Jonathan Beltrão',
            'email' => 'supplier@gmail.com',
            'monthly' => '15.00'
        ]);
        $this->assertIsObject($supplier);
        $this->assertInstanceOf('\App\Supplier', $supplier);
    }

    public function testUpdate()
    {
        $supplierService = new SupplierService();
        $userService = new UserService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => '123456',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'cnpj' => '34384271093',
            'phone' => '83998557854',
            'zipcode' => '58010-270'
        ]);

        $supplier = $supplierService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'supplier@gmail.com',
            'monthly' => '15,00',
        ], $user->company_id);

        $supplierService->update([
            'name' => 'Jonas Beltras',
            'email' => 'sups@gmail.com',
            'monthly' => '30,50'
        ],$supplier->id, $user->company_id);

        $this->assertDatabaseHas('suppliers', [
            'name' => 'Jonas Beltras',
            'email' => 'sups@gmail.com',
            'monthly' => 30.50
        ]);
        $this->assertIsObject($supplier);
        $this->assertInstanceOf('\App\Supplier', $supplier);
    }

    public function testShow()
    {
        $supplierService = new SupplierService();
        $userService = new UserService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => '123456',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'cnpj' => '34384271093',
            'phone' => '83998557854',
            'zipcode' => '58010-270'
        ]);

            $supplierRecord = $supplierService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'supplier@gmail.com',
            'monthly' => '15,00',
        ], $user->company_id);

        $supplier = $supplierService->show($supplierRecord->id);

        $this->assertIsObject($supplier);
        $this->assertInstanceOf('\App\Supplier', $supplier);
    }

    public function testList()
    {
        $supplierService = new SupplierService();
        $userService = new UserService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => '123456',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'cnpj' => '34384271093',
            'phone' => '83998557854',
            'zipcode' => '58010-270'
        ]);

        for($i = 0; $i <= 10; $i++) {
            $supplierService->create([
                'name' => 'Jonathan Beltrão',
                'email' => "jonas{$i}@gmail.com",
                'monthly' => '15,00',
            ], $user->company_id);
        }

        $suppliers = $supplierService->list($user->company_id, null);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $suppliers);
        $this->assertNotNull($suppliers);
        $this->assertCount(11, $suppliers);
    }

    public function testTotalAmountMonthly()
    {
        $supplierService = new SupplierService();
        $userService = new UserService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => '123456',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'cnpj' => '34384271093',
            'phone' => '83998557854',
            'zipcode' => '58010-270'
        ]);

        for($i = 0; $i <= 10; $i++) {
            $supplierService->create([
                'name' => 'Jonathan Beltrão',
                'email' => "jonas{$i}@gmail.com",
                'monthly' => '15,00',
            ], $user->company_id);
        }

        $response = $supplierService->totalAmountMonthly($user->company_id);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('quantity', $response);
        $this->assertArrayHasKey('total_amount', $response);
        $this->assertEquals(11, $response['quantity']);
        $this->assertEquals(165, $response['total_amount']);
    }

    public function testDelete()
    {
        $supplierService = new SupplierService();
        $userService = new UserService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => '123456',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'cnpj' => '34384271093',
            'phone' => '83998557854',
            'zipcode' => '58010-270'
        ]);

        $supplier = $supplierService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'supplier@gmail.com',
            'monthly' => '15,00',
        ], $user->company_id);

        $supplier = $supplierService->delete($supplier->id);

        $this->assertSoftDeleted('suppliers', [
            'name' => 'Jonathan Beltrão',
            'email' => 'supplier@gmail.com',
            'monthly' => '15.00'
        ]);
        $this->assertIsObject($supplier);
        $this->assertInstanceOf('\App\Supplier', $supplier);
    }
}
