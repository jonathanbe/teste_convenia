<?php

namespace Tests\Feature;

use App\Services\Api\SupplierService;
use App\Services\UserService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SupplierEndpointsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp() : void
    {
        parent::setUp();
        $this->artisan('passport:install');
    }

    public function testListEndpoint()
    {
        $userService = new UserService();
        $supplierService = new SupplierService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jona@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'cnpj' => '58.184.643/0001-12',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'phone' => '(83)99855-7854',
            'zipcode' => '58010-270'
        ]);

        $login = $this->json('POST','/api/login', [
            'email' => 'jona@gmail.com',
            'password' => '12345678',
        ]);

        $loginContent = $login->decodeResponseJson();


        for($i = 0; $i <= 10; $i++) {
            $supplierService->create([
                'name' => 'Jonathan Beltrão',
                'email' => "jonas{$i}@gmail.com",
                'monthly' => '15,00',
            ], $user->company_id);
        }

        $list = $this->json('GET', '/api/suppliers', [], [
            'Authorization' => $loginContent['token_type'] . " " . $loginContent['access_token']
        ]);

        $list->assertStatus(200);
        $list->assertJsonStructure(
            [
                [
                    'name',
                    'email',
                    'monthly',
                    'activated',
                    'created_at',
                    'updated_at',
                    'deleted_at'
                ]
            ]
        );
    }

    public function testCreateEndpoint()
    {
        $userService = new UserService();

        $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jona@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'cnpj' => '58.184.643/0001-12',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'phone' => '(83)99855-7854',
            'zipcode' => '58010-270'
        ]);

        $login = $this->json('POST','/api/login', [
            'email' => 'jona@gmail.com',
            'password' => '12345678',
        ]);

        $loginContent = $login->decodeResponseJson();

        $data = [
            'name' => 'Jonathan Beltrão',
            'email' => "jonas@gmail.com",
            'monthly' => '15,00',
        ];

        $create = $this->json('POST', '/api/supplier/create', $data, [
            'Authorization' => $loginContent['token_type'] . " " . $loginContent['access_token']
        ]);

        $create->assertStatus(200);
        $create->assertJsonStructure(
            [
                'name',
                'email',
                'monthly',
                'activated',
                'created_at',
                'updated_at',
                'deleted_at'
            ]
        );
    }

    public function testUpdateEndpoint()
    {
        $userService = new UserService();
        $supplierService = new SupplierService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jona@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'cnpj' => '58.184.643/0001-12',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'phone' => '(83)99855-7854',
            'zipcode' => '58010-270'
        ]);

        $supplier = $supplierService->create([
            'name' => 'Jonathan Beltrão',
            'email' => "jonas@gmail.com",
            'monthly' => '15,00',
        ], $user->company_id);

        $login = $this->json('POST','/api/login', [
            'email' => 'jona@gmail.com',
            'password' => '12345678',
        ]);

        $loginContent = $login->decodeResponseJson();

        $data = [
            'name' => 'Jonathans Beltrão',
            'monthly' => '25,50',
        ];

        $update = $this->json('PATCH', "/api/supplier/edit/{$supplier->id}", $data, [
            'Authorization' => $loginContent['token_type'] . " " . $loginContent['access_token']
        ]);

        $update->assertStatus(200);
        $update->assertJsonStructure(
            [
                'name',
                'email',
                'monthly',
                'activated',
                'created_at',
                'updated_at',
                'deleted_at'
            ]
        );
        $update->assertJson([
            'name' => 'Jonathans Beltrão',
            'email' => "jonas@gmail.com",
            'monthly' => '25.50',
        ]);
    }

    public function testShowEndpoint()
    {
        $userService = new UserService();
        $supplierService = new SupplierService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jona@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'cnpj' => '58.184.643/0001-12',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'phone' => '(83)99855-7854',
            'zipcode' => '58010-270'
        ]);

        $login = $this->json('POST','/api/login', [
            'email' => 'jona@gmail.com',
            'password' => '12345678',
        ]);

        $loginContent = $login->decodeResponseJson();

        $supplier = $supplierService->create([
            'name' => 'Jonathan Beltrão',
            'email' => "jonas@gmail.com",
            'monthly' => '15,00',
        ], $user->company_id);


        $show = $this->json('GET', "/api/supplier/{$supplier->id}", [], [
            'Authorization' => $loginContent['token_type'] . " " . $loginContent['access_token']
        ]);

        $show->assertStatus(200);
        $show->assertJsonStructure(
            [
                'name',
                'email',
                'monthly',
                'activated',
                'created_at',
                'updated_at',
                'deleted_at'
            ]
        );

        $show->assertJson([
            'name' => 'Jonathan Beltrão',
            'email' => "jonas@gmail.com",
            'monthly' => '15.00',
            'activated' => 0
        ]);
    }

    public function testDeleteEndpoint()
    {
        $userService = new UserService();
        $supplierService = new SupplierService();

        $user = $userService->create([
            'name' => 'Jonathan Beltrão',
            'email' => 'jona@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'cnpj' => '58.184.643/0001-12',
            'company_name' => 'Joninha Company',
            'address' => 'Rua Prefeito Osvaldo Pessoa, 404',
            'phone' => '(83)99855-7854',
            'zipcode' => '58010-270'
        ]);

        $login = $this->json('POST','/api/login', [
            'email' => 'jona@gmail.com',
            'password' => '12345678',
        ]);

        $loginContent = $login->decodeResponseJson();

        $supplier = $supplierService->create([
            'name' => 'Jonathan Beltrão',
            'email' => "jonas@gmail.com",
            'monthly' => '15,00',
        ], $user->company_id);


        $delete = $this->json('DELETE', "/api/supplier/{$supplier->id}", [], [
            'Authorization' => $loginContent['token_type'] . " " . $loginContent['access_token']
        ]);

        $show = $this->json('GET', "/api/supplier/{$supplier->id}", [], [
            'Authorization' => $loginContent['token_type'] . " " . $loginContent['access_token']
        ]);
        $show->assertStatus(401);

        $delete->assertStatus(200);
        $delete->assertJsonStructure(
            [
                'name',
                'email',
                'monthly',
                'activated',
                'created_at',
                'updated_at',
                'deleted_at'
            ]
        );
        $delete->assertJson([
            'name' => 'Jonathan Beltrão',
            'email' => "jonas@gmail.com",
            'monthly' => '15.00',
            'activated' => 0
        ]);
    }
}
