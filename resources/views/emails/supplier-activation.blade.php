@component('mail::message')
Olá, {{ $userName }}

Clique no link abaixo para ativar o cadastro do fornecedor <strong>{{ $supplierName }}</strong>
Cadastrado na sua empresa <strong>{{ $companyName }}</strong>.

@component('mail::button', ['url' => $url])
Ativar Cadastro
@endcomponent

Obrigado,<br>
<h2>ConveniaTest</h2>
@endcomponent
