@component('mail::message')
Olá, <strong>{{$userName}}</strong>

Como medida de segurança, viemos te avisar que
o fornecedor <strong>{{$supplierName}}</strong> foi excluido.

Atenciosamente,<br>
<h2>ConveniaTest</h2>
@endcomponent
