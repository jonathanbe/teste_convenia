@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card p-4">
                <div class="card-<strong>Body</strong>">
                    <h1 >Convenia Test API Docs</h1>

                    <h3 class="mt-5">Usage</h3>

                    <div class="blockquote-footer" style="font-size: 14.5px;">
                        <p><strong>/api/register</strong> (Register User/Company)</p>
                        <ul>
                            <li>Method: POST</li>
                            <li>Parameters</li>
                                <ul><strong>Company</strong>
                                    <li>company_name (string|required)</li>
                                    <li>cnpj (string|required|unique|format: 00.000.000/0000-00)</li>
                                    <li>phone (string|required|format: (00)00000-0000)</li>
                                    <li>address (string|required)</li>
                                    <li>zipcode (string|required|format: 00000-000)</li>
                                </ul>
                                <ul><strong>User</strong>
                                    <li>name (string|required)</li>
                                    <li>email (string|required|unique)</li>
                                    <li>password (string|required)</li>
                                    <li>password_confirmation (string|required)</li>
                                </ul>
                            <li>Response (application/json)</li>
                                <ul>
                                    <li>token</li>
                                    <li>token_type</li>
                                </ul>
                        </ul>

                        <p><strong>/api/login</strong> (Login User)</p>
                        <ul>
                            <li>Method: POST</li>
                            <li>Parameters</li>
                            <ul>
                                <li>email (string|required)</li>
                                <li>password (string|required)</li>
                            </ul>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>token</li>
                                <li>token_type</li>
                            </ul>
                        </ul>

                        <p><strong>/api/logout</strong> (Logout User)</p>
                        <ul>
                            <li>Method: GET</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                                </ul>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>string ('Token Desabilitado')</li>
                            </ul>
                        </ul>

                        <p><strong>/api/user</strong> (User Information)</p>
                        <ul>
                            <li>Method: GET</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                            </ul>
                            <li>Response <small>(application/json)</small></li>
                            <ul>
                                <li>id</li>
                                <li>name</li>
                                <li>email</li>
                                <li>created_at</li>
                                <li>updated_at</li>
                                <li>company {id, name, cnpj, phone, address, zipcode, created_at, updated_at</li>
                            </ul>
                        </ul>

                        <h5 class="mt-5">Authenticated</h5>

                        <p><strong>/api/suppliers/{offset:[optional]}</strong> (List all Suppliers)</p>
                        <ul>
                            <li>Method: GET</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                            </ul>
                            <li>Response <small>(Collection: application/json)</small>  </li>
                            <ul>
                                <li>id</li>
                                <li>name</li>
                                <li>email</li>
                                <li>monthly</li>
                                <li>activated</li>
                                <li>created_at</li>
                                <li>updated_at</li>
                                <li>deleted_at</li>
                            </ul>
                        </ul>

                        <p><strong>/api/supplier/total-amount</strong> (List Company total monthly amount)</p>
                        <ul>
                            <li>Method: GET</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                            </ul>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>quantity</li>
                                <li>total_amount</li>
                            </ul>
                        </ul>

                        <p><strong>/api/supplier/create</strong> (Create new Supplier)</p>
                        <ul>
                            <li>Method: POST</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                            </ul>
                            <ul><strong>Body</strong>
                                <li>name (string|required)</li>
                                <li>email (string|required|unique)</li>
                                <li>monthly (string|required|format: 00000,00)</li>
                            </ul>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>id</li>
                                <li>name</li>
                                <li>email</li>
                                <li>monthly (format: 00000.00)</li>
                                <li>activated</li>
                                <li>created_at</li>
                                <li>updated_at</li>
                                <li>deleted_at</li>
                            </ul>
                        </ul>

                        <p><strong>/api/supplier/edit/{id}</strong> (Update Supplier)</p>
                        <ul>
                            <li>Method: PATCH</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                            </ul>
                            <ul><strong>Body</strong>
                                <li>name (string)</li>
                                <li>email (string|unique)</li>
                                <li>monthly (string|format: 00000,00)</li>
                            </ul>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>id</li>
                                <li>name</li>
                                <li>email</li>
                                <li>monthly (format: 00000.00)</li>
                                <li>activated</li>
                                <li>created_at</li>
                                <li>updated_at</li>
                                <li>deleted_at</li>
                            </ul>
                        </ul>

                        <p><strong>/api/supplier/show/{id}</strong> (Show Supplier)</p>
                        <ul>
                            <li>Method: GET</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                            </ul>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>id</li>
                                <li>name</li>
                                <li>email</li>
                                <li>monthly (format: 00000.00)</li>
                                <li>activated</li>
                                <li>created_at</li>
                                <li>updated_at</li>
                                <li>deleted_at</li>
                            </ul>
                        </ul>

                        <p><strong>/api/supplier/{id}</strong> (Delete Supplier)</p>
                        <ul>
                            <li>Method: DELETE</li>
                            <li>Parameters</li>
                            <ul><strong>Headers</strong>
                                <li>Authorization(Bearer {token}</li>
                            </ul>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>id</li>
                                <li>name</li>
                                <li>email</li>
                                <li>monthly (format: 00000.00)</li>
                                <li>activated</li>
                                <li>created_at</li>
                                <li>updated_at</li>
                                <li>deleted_at</li>
                            </ul>
                        </ul>

                        <p><strong>/api/supplier/activate/{id}</strong> (Activate Supplier)</p>
                        <ul>
                            <li>Method: GET</li>
                            <li>Response (application/json)</li>
                            <ul>
                                <li>string ('Ativado com sucesso')</li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
