<?php

namespace App\Policies;

use App\User;
use App\Supplier;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupplierPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Supplier $supplier)
    {
        return $user->company_id == $supplier->company_id;
    }

    public function update(User $user, Supplier $supplier)
    {
        return $user->company_id == $supplier->company_id;
    }

    public function delete(User $user, Supplier $supplier)
    {
        return $user->company_id == $supplier->company_id;
    }

}
