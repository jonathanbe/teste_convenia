<?php

namespace App\Mail;

use App\Company;
use App\Supplier;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeletedSupplier extends Mailable
{
    use Queueable, SerializesModels;

    public $supplier;
    public $user;
    public $company;

    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
        $this->company = $this->supplier->company;
        $this->user = $this->supplier->company->user;
    }

    public function build()
    {
        return $this->markdown('emails.supplier-deleted')
            ->subject('Fornecedor Excluido')
            ->with([
                    'userName' => $this->user->name,
                    'companyName' => $this->company->name,
                    'supplierName' => $this->supplier->name,
                ]
            );
    }
}
