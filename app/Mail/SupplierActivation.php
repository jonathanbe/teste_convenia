<?php

namespace App\Mail;

use App\Company;
use App\Supplier;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupplierActivation extends Mailable
{
    use Queueable, SerializesModels;

    public $supplier;
    public $company;
    public $user;

    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
        $this->company = $supplier->company;
        $this->user = $supplier->company->user;
    }

    public function build()
    {
        $urlString = '/api/supplier/activate/' . $this->supplier->id;

        return $this->markdown('emails.supplier-activation')
            ->subject('Ativação do Cadastro')
            ->with([
                'userName' => $this->user->name,
                'companyName' => $this->company->name,
                'supplierName' => $this->supplier->name,
                'url' => url($urlString)
            ]
        );
    }
}
