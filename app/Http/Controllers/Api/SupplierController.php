<?php

namespace App\Http\Controllers\Api;

use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Api\SupplierService;
use App\Http\Requests\Api\SupplierCreateRequest;
use App\Http\Requests\Api\SupplierEditRequest;
use App\Http\Resources\Supplier as SupplierResource;

class SupplierController extends Controller
{
    protected $service;
    protected $loggedUser;
    protected $supplier;

    public function __construct(SupplierService $service)
    {
        $this->service = $service;
        $this->middleware(function ($request, $next){
            $this->loggedUser = request()->user();

            return $next($request);
        });
    }

    public function index($offset = null)
    {
        $suppliers = SupplierResource::collection(
            $this->service->list(
                $this->loggedUser->company_id,
                $offset
            )
        );

        return response($suppliers, 200);
    }

    public function store(SupplierCreateRequest $request)
    {
        $supplier = new SupplierResource(
            $this->service->create(
                $request->all(),
                $this->loggedUser->company_id
            )
        );

        return response($supplier, 200);
    }

    public function show($id)
    {
        if ($this->loggedUser->can('view', Supplier::find($id))) {
            $supplier = new SupplierResource(
                $this->service->show(
                    $id,
                    $this->loggedUser->company_id
                )
            );

            return response($supplier, 200);
        }

        return response('Não Autorizado', 401);
    }

    public function update(SupplierEditRequest $request, $id)
    {
        if ($this->loggedUser->can('update', Supplier::find($id))) {
            $supplier = new SupplierResource(
                $this->service->update(
                    $request->all(),
                    $id,
                    $this->loggedUser->company_id
                )
            );

            return response($supplier, 200);
        }

        return response('Não Autorizado', 401);
    }

    public function destroy($id)
    {
        if ($this->loggedUser->can('delete', Supplier::find($id))) {
            $supplier = new SupplierResource(
                $this->service->delete($id)
            );

            return response($supplier, 200);
        }
        return response('Não Autorizado', 401);
    }

    public function totalAmountMonthly()
    {
        $totalAmountMonthly = $this->service->totalAmountMonthly($this->loggedUser->company_id);

        return response($totalAmountMonthly, 200);
    }

    public function activate($id)
    {
        $this->service->activateSupplier($id);

        return view('suppliers.activation');
    }
}
