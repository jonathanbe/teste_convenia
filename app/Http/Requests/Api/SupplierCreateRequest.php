<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SupplierCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:suppliers',
            'monthly' => ['required', 'regex:/^(\d{1,3}(\.\d{3})*|\d+)(\,\d{2})?$/']
        ];
    }

    public function messages()
    {
        return [
            'required' => 'O campo é obrigatório',

            'email' => 'Por favor digite um email válido',
            'email.unique' => 'Este email já existe',

            'monthly.regex' => 'Formato de moeda não aceito'
        ];
    }
}
