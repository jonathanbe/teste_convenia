<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'company_name' => 'required',
            'cnpj' => 'required|cnpj|unique:companies',
            'phone' => 'required|celular_com_ddd',
            'address' => 'required',
            'zipcode' => 'required|formato_cep'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'O campo é obrigatório',

            'name.min' => 'O campo nome precisa ter ao menos 2 caracteres',

            'email' => 'Por favor digite um email válido',
            'email.unique' => 'Este email já existe',

            'password.min' => 'O campo precisa ter ao menos 8 caracteres',
            'password.confirm' => 'Os valores de Senha precisam ser iguais',

            'zipcode.formato_cep' => 'Por favor digite um CEP válido'
        ];
    }
}
