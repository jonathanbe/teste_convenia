<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SupplierEditRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'email|unique:suppliers',
            'monthly' => ['regex:/^(\d{1,3}(\.\d{3})*|\d+)(\,\d{2})?$/']
        ];
    }

    public function messages()
    {
        return [
            'email' => 'Por favor digite um email válido',
            'email.unique' => 'Este email já existe',

            'monthly.regex' => 'Formato de moeda não aceito'
        ];
    }
}
