<?php

namespace App\Listeners;

use App\Mail\DeletedSupplier;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendDeletedMailListener
{
    public function handle($event)
    {
        Mail::to($event->user->email, $event->user->name)
            ->later(5 ,new DeletedSupplier($event->supplier));
    }
}
