<?php

namespace App\Listeners;

use App\Mail\SupplierActivation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendActivationMailListener
{
    public function handle($event)
    {
        Mail::to($event->user->email, $event->user->name)
            ->later(5 ,new SupplierActivation($event->supplier));
    }
}
