<?php


namespace App\Services\Api;

use App\Company;
use App\Events\CreatedSupplierEvent;
use App\Events\DeletedSupplierEvent;
use App\Mail\DeletedSupplier;
use App\User;
use App\Mail\SupplierActivation;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use App\Supplier;

class SupplierService
{
    public function create(Array $data, $company_id)
    {
        $supplier = Supplier::create([
            'company_id' => $company_id,
            'name' => $data['name'],
            'email' => $data['email'],
            'monthly' => $data['monthly']
        ]);
        event(new CreatedSupplierEvent($supplier));

        return $supplier;
    }

    public function list($company_id, $offset = null)
    {
        $cacheCompanyKey = 'company_id';
        $storedCompanyId = Cache::get($cacheCompanyKey);

        if(!$storedCompanyId) {
            Cache::forever($cacheCompanyKey, $company_id);
        }

        if($storedCompanyId  != $company_id) {
            Cache::flush();
        }

        $cacheKey = "suppliers_{$offset}";
        $suppliers = Cache::get($cacheKey);

        if(!$suppliers) {
            if($offset != null) {
                $suppliers = Supplier::query()
                    ->where('company_id', $company_id)
                    ->orderBy('id')
                    ->skip($offset * 4)
                    ->take(4)
                    ->get();
            } else {
                $suppliers = Supplier::query()
                    ->where('company_id', $company_id)
                    ->orderBy('id')
                    ->get();
            }

            Cache::forever($cacheKey, $suppliers);
        }

        return $suppliers;
    }

    public function update(Array $data, $id, $company_id)
    {
        $supplier = Supplier::where('id', $id)->first();
        if($supplier->company_id != $company_id) {

        } else {
            $supplier->update($data);

            return Supplier::find($id);
        }
    }

    public function show($id) : Supplier
    {
        $company_id = request()->user()->company_id;
        $cacheCompanyKey = 'company_id';
        $storedCompanyId = Cache::get($cacheCompanyKey);

        if(!$storedCompanyId) {
            Cache::forever($cacheCompanyKey, $company_id);
        }

        if($storedCompanyId  != $company_id) {
            Cache::flush();
        }

        $cacheKey = "show";
        $supplier = Cache::get($cacheKey);

        if(!$supplier) {
            $supplier = Supplier::find($id);
            Cache::forever($cacheKey, $supplier);
        }

        return $supplier;
    }

    public function delete($id)
    {
        $supplier = Supplier::where('id', $id)->first();
        $supplier->delete();

        event(new DeletedSupplierEvent($supplier));

        return $supplier;
    }

    public function totalAmountMonthly($company_id)
    {
        $company_id = request()->user()->company_id;
        $cacheCompanyKey = 'company_id';
        $storedCompanyId = Cache::get($cacheCompanyKey);

        if(!$storedCompanyId) {
            Cache::forever($cacheCompanyKey, $company_id);
        }

        if($storedCompanyId  != $company_id) {
            Cache::flush();
        }

        $cacheKey = "total_amount";
        $response = Cache::get($cacheKey);

        if(!$response) {
            $supplierCollection = Supplier::where('company_id', $company_id)->get();
            $response = [
                'quantity' => $supplierCollection->count(),
                'total_amount' => $supplierCollection->sum('monthly')
            ];
            Cache::forever($cacheKey, $response);
        }

        return $response;
    }

    public function activateSupplier($id)
    {
        $supplier = Supplier::find($id);
        $supplier->activated = 1;
        $supplier->save();
    }
}
