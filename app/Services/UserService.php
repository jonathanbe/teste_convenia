<?php


namespace App\Services;


use App\User;
use App\Company;
use Illuminate\Support\Facades\DB;

class UserService
{
    public function create(Array $data) : User
    {
        DB::beginTransaction();
            $company = $this->createCompany($data);
            $user = $this->createUser($company, $data);
        DB::commit();

        return $user;
    }
    
    public function createCompany(Array $data) : Company
    {
        $company = Company::create([
            'name' => $data['company_name'],
            'cnpj' => $data['cnpj'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'zipcode' => $data['zipcode'],
        ]);

        return $company;
    }

    function createUser(Company $company, $data): User
    {
        return User::create([
            'name' => $data['name'],
            'company_id' => $company->id,
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
    }
}
