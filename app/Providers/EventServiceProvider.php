<?php

namespace App\Providers;

use App\Events\CreatedSupplierEvent;
use App\Events\DeletedSupplierEvent;
use App\Listeners\SendActivationMailListener;
use App\Listeners\SendDeletedMailListener;
use App\Mail\DeletedSupplier;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        DeletedSupplierEvent::class => [
            SendDeletedMailListener::class,
        ],
        CreatedSupplierEvent::class => [
            SendActivationMailListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
