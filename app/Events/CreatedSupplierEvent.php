<?php

namespace App\Events;

use App\Supplier;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreatedSupplierEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $supplier;
    public $company;
    public $user;

    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
        $this->company = $supplier->company;
        $this->user = $supplier->company->user;
    }
}
