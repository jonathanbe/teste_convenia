<?php

namespace App\Events;

use App\Supplier;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DeletedSupplierEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $supplier;
    public $user;

    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
        $this->user = $supplier->company->user;
    }
}
