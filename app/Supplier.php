<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Company;
use Illuminate\Database\Eloquent\SoftDeletes;


class Supplier extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'company_id',
        'name',
        'email',
        'monthly'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function setMonthlyAttribute($monthly)
    {
        $this->attributes['monthly'] = str_replace(',','.', str_replace('.','', $monthly));
    }
}
