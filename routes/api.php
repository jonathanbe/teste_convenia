            <?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function(){
    // Supplier Routes
    Route::get('/suppliers/{offset?}', 'Api\SupplierController@index')->name('listAll');
    Route::get('/supplier/total-amount', 'Api\SupplierController@totalAmountMonthly')->name('returnTotalMonthlyAmount');
    Route::post('/supplier/create', 'Api\SupplierController@store')->name('createSupplier');
    Route::patch('/supplier/edit/{id}', 'Api\SupplierController@update')->name('updateSupplier');
    Route::get('/supplier/{id}', 'Api\SupplierController@show')->name('showSupplier');
    Route::delete('/supplier/{id}', 'Api\SupplierController@destroy')->name('deleteSupplier');

    // User Routes
    Route::get('/logout', 'Api\AuthController@logout')->name('logout')->name('logout');
    Route::get('/user', 'Api\AuthController@user')->name('loggedUser');
});
// Activate Supplier
Route::get('/supplier/activate/{id}', 'Api\SupplierController@activate')->name('activateSupplier');

// Create/Login User
Route::post('/register', 'Api\AuthController@register')->name('createUser');
Route::post('/login', 'Api\AuthController@login')->name('login');

